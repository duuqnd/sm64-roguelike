# Super Mario 64 Roguelike



A rom hack for Super Mario 64 that turns it into a roguelike.
In this hack you can't regain health once you lose it, you can't save, and you have only one life.

### The current version is 06
The version number is displayed on the main menu.

# Getting a ROM
A ROM with this hack can be made in two different ways.

## Method 1 (recommended):
Download the BPS patch `roguelike.bps` from this repo and apply it to an american SM64 ROM.

## Method 2:
Download the git patch `roguelike.patch`, clone the SM64 Decompilation project, patch the source code with `git patch`, then compile the game.

# Additional Content

Pressing the L button will disable all music until the game is restarted.

There are three difficulty settings.

* Vanilla, most things work like in the normal game.
* Normal, some enemies are (barely) more powerful, and a few other tweaks are there.
* Hard, Mario keeps walking for a while after you let go of the joystick,
you have half of the normal health, some enemies are actually dangerous now and a few other things.
Recommended for skilled players and masocists.

# TODO

* Add in-game credits screen.
